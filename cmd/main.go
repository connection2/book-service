package main

import (
	"net"
	"temp/config"
	pb "temp/genproto/book"
	"temp/pkg/db"
	"temp/pkg/logger"
	"temp/service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "book-service")
	defer logger.Cleanup(log)

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase))

	connDB, err := db.ConnectToDB(cfg)
	if err != nil{
		log.Fatal("sqlx connection to postgres error",logger.Error(err))
	}

	bookService := service.NewBookService(connDB, log)

	lis, err := net.Listen("tcp", cfg.RPCPort)
	if err != nil{
		log.Fatal("Error while listening",logger.Error(err))
	}
	s := grpc.NewServer()
	reflection.Register(s)
	pb.RegisterBookServiceServer(s, bookService)
	log.Info("main: server running",
	logger.String("port",cfg.RPCPort))
	if err := s.Serve(lis); err != nil{
		log.Fatal("Error while listening: %v",logger.Error(err))
	}

}