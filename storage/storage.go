package storage

import (
	"temp/storage/postgres"
	"temp/storage/repo"
	"github.com/jmoiron/sqlx"
)
type IStorage interface{
	Book() repo.BookStorage
}
type storagePg struct{
	db *sqlx.DB
	bookRepo repo.BookStorage
}
func NewStoragePg(db *sqlx.DB) *storagePg{
	return &storagePg{
		db: db,
		bookRepo: postgres.NewBookRepo(db),
	}
}
func (s storagePg) Book() repo.BookStorage{
	return s.bookRepo
}