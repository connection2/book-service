package repo

import pb "temp/genproto/book"

type BookStorage interface{
	CreateBook(*pb.Book) (*pb.Book, error)
	GetBookID(bookID int64) (*pb.Book, error)
}