package postgres

import (
	pb "temp/genproto/book"
	"github.com/jmoiron/sqlx"
)

type bookRepo struct{
	db *sqlx.DB
}

func NewBookRepo(db *sqlx.DB) *bookRepo{
	return &bookRepo{db: db}
}

func (b *bookRepo) CreateBook(req *pb.Book) (*pb.Book, error) {
	
	books := pb.Book{}
	err := b.db.QueryRow(`insert into book(name, author, price) values($1, $2, $3) 
	returning id, name, author, price`,req.Name,req.Author,req.Price).Scan(&books.Id, &books.Name, &books.Author, &books.Price)
	if err != nil{
		return &pb.Book{}, err
	}
return &books, nil

}
func (b *bookRepo) GetBookID(bookID int64) (*pb.Book, error){
	books := pb.Book{}
	err := b.db.QueryRow(`select id, name, author, price from book where
	id = $1`,bookID).Scan(&books.Id, &books.Name, &books.Author, &books.Price)
	if err != nil{
		return &pb.Book{}, err
	}
return &books, nil
}

