package client

import "temp/config"

//GrpcClientI ...
type GrpcClientI interface{}	

//GrpcClient...
type GrpcClient struct{
	cfg config.Config
}
//New 
func New(cfg config.Config) (*GrpcClient, error){
	return &GrpcClient{
		cfg: cfg,
	},nil
}

