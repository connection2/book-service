package service

import (
	"context"
	pb "temp/genproto/book"
	"temp/pkg/logger"
	"temp/storage"
	"github.com/jmoiron/sqlx"
		"google.golang.org/grpc/codes"
		"google.golang.org/grpc/status"
)

type BookService struct{
	storage storage.IStorage
	logger logger.Logger
}

func NewBookService(db *sqlx.DB, log logger.Logger) *BookService{
	return &BookService{
		storage:  storage.NewStoragePg(db),
		logger: log,
	}
}
func (b *BookService) CreateBook(ctx context.Context, req *pb.Book) (*pb.Book, error) {
	book, err := b.storage.Book().CreateBook(req)
	if err != nil{
			b.logger.Error("Error with Book",logger.Any("error insert Book",err))
			return &pb.Book{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}
	return book, nil
}

func (b *BookService) GetBookID(ctx context.Context, req *pb.GetBook) (*pb.Book, error) {
	book, err := b.storage.Book().GetBookID(req.Id)
	if err != nil{
		b.logger.Error("Error with Book",logger.Any("error insert Book",err))
		return &pb.Book{}, status.Error(codes.Internal,"something went wrong, please check product info")
	}
	return book, nil
}